ShopifyApp.configure do |config|
  config.application_name = "My Shopify App"
  config.api_key = "d8d61e14e62f72d7258b6613ad9ef7e7"
  config.secret = "ff5d6ca9df81011970c51a6caea7d783"
  config.scope = "read_products" # Consult this page for more scope options:
                                 # https://help.shopify.com/en/api/getting-started/authentication/oauth/scopes
  config.embedded_app = true
  config.after_authenticate_job = false
  config.session_repository = Shop
end
